Pod::Spec.new do |s|
  s.name = "RoShamBoCore"
  s.version = "1.0.0"
  s.summary = "Micro Feature"
  s.description = <<-DESC
                       RoShamBoCore is resposible for ...
                       DESC
  s.homepage = "https://db-in.com"
  s.documentation_url = "https://git.db-in.com/projects/EMA/repos/ek-mobile-ios-app-swift"
  s.license = { :type => "db-in License", :file => "LICENSE" }
  s.author = 'db-in'
  s.source = { :git => "https://bitbucket.org/dineybomfim/roshambo-core.git", :tag => "RELEASE-#{s.version}", :submodules => true }
  s.swift_version = '4.1'

s.requires_arc = true
s.ios.deployment_target = '10.0'
s.osx.deployment_target = '10.10'
s.tvos.deployment_target = '9.0'
s.watchos.deployment_target = '2.0'

  s.public_header_files = 'RoShamBoCore/**/*.h'
  s.source_files = 'RoShamBoCore/**/*.{h,m,swift}'
  s.exclude_files = 'RoShamBoCore/**/Info.plist'

  s.ios.frameworks = 'Foundation'
  s.dependency 'MarkovModel', '~> 1.0.0'

end
