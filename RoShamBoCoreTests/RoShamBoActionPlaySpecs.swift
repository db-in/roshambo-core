/*
 *	RoShamBoActionPlaySpec.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/25/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Quick
import Nimble
@testable import RoShamBoCore

// MARK: - Definitions -

// MARK: - Type -

class RoShamBoActionPlaySpec : QuickSpec {

// MARK: - Properties

// MARK: - Protected Methods

// MARK: - Exposed Methods

// MARK: - Overridden Methods

	override func spec() {

		describe("Testing RoShamBoPlay") {

			context("With rock play") {
				
				it("Should beat scissors") {
					expect(RoShamBoAction.rock > .scissors).to(beTruthy())
					expect(RoShamBoAction.scissors > .rock).to(beFalsy())
				}
				
				it("Should lose to paper") {
					expect(RoShamBoAction.rock < .paper).to(beTruthy())
					expect(RoShamBoAction.paper < .rock).to(beFalsy())
				}
				
				it("Should tie against rock") {
					expect(RoShamBoAction.rock < .rock).to(beFalsy())
					expect(RoShamBoAction.rock > .rock).to(beFalsy())
				}
			}
			
			context("With paper play") {
				
				it("Should beat rock") {
					expect(RoShamBoAction.paper > .rock).to(beTruthy())
					expect(RoShamBoAction.rock > .paper).to(beFalsy())
				}
				
				it("Should lose to scissors") {
					expect(RoShamBoAction.paper < .scissors).to(beTruthy())
					expect(RoShamBoAction.scissors < .paper).to(beFalsy())
				}
				
				it("Should tie against paper") {
					expect(RoShamBoAction.paper > .paper).to(beFalsy())
					expect(RoShamBoAction.paper < .paper).to(beFalsy())
				}
			}
			
			context("With scissors play") {
				
				it("Should beat paper") {
					expect(RoShamBoAction.scissors > .paper).to(beTruthy())
					expect(RoShamBoAction.paper > .scissors).to(beFalsy())
				}
				
				it("Should lose to rock") {
					expect(RoShamBoAction.scissors < .rock).to(beTruthy())
					expect(RoShamBoAction.rock < .scissors).to(beFalsy())
				}
				
				it("Should tie against scissors") {
					expect(RoShamBoAction.scissors > .scissors).to(beFalsy())
					expect(RoShamBoAction.scissors < .scissors).to(beFalsy())
				}
			}
		}
	}
}
