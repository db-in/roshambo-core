/*
 *	RoShamBoAISpec.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 7/25/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Quick
import Nimble
@testable import RoShamBoCore

// MARK: - Definitions -

// MARK: - Type -

class RoShamBoAISpec : QuickSpec {

// MARK: - Properties

// MARK: - Protected Methods

// MARK: - Exposed Methods

// MARK: - Overridden Methods

	override func spec() {

		describe("Testing RoShamBo AI") {

			context("With smart prediction algorithms") {

				it("WHY is this test being done, explain the expected behavior/result") {
					let ai = RoShamBoAI()
					ai.play(against: .paper)
					ai.play(against: .rock)
					ai.play(against: .paper)
					ai.play(against: .rock)
					ai.play(against: .paper)
					ai.play(against: .scissors)
				}
			}
		}
	}
}
