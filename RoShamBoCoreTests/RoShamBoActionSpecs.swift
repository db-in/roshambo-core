/*
 *	RoShamBoActionSpec.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/25/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Quick
import Nimble
@testable import RoShamBoCore

// MARK: - Definitions -

// MARK: - Type -

class RoShamBoActionSpec : QuickSpec {

// MARK: - Properties

// MARK: - Protected Methods

// MARK: - Exposed Methods

// MARK: - Overridden Methods

	override func spec() {

		describe("Testing RoShamBoAction") {

			context("With all cases") {
				
				it("Should be rock, paper, scissors") {
					let allCases: [RoShamBoAction] = [.rock, .paper, .scissors]
					expect(RoShamBoAction.allCases).to(equal(allCases))
				}
			}
			
			context("With rock") {

				it("Should win from scissors") {
					expect(RoShamBoAction.rock.winsFrom).to(equal(RoShamBoAction.scissors))
				}
				
				it("Should lose to paper") {
					expect(RoShamBoAction.rock.losesTo).to(equal(RoShamBoAction.paper))
				}
			}
			
			context("With scissors") {
				
				it("Should win from paper") {
					expect(RoShamBoAction.scissors.winsFrom).to(equal(RoShamBoAction.paper))
				}
				
				it("Should lose to rock") {
					expect(RoShamBoAction.scissors.losesTo).to(equal(RoShamBoAction.rock))
				}
			}
			
			context("With paper") {
				
				it("Should win from rock") {
					expect(RoShamBoAction.paper.winsFrom).to(equal(RoShamBoAction.rock))
				}
				
				it("Should lose to scissors") {
					expect(RoShamBoAction.paper.losesTo).to(equal(RoShamBoAction.scissors))
				}
			}
		}
	}
}
