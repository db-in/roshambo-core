/*
 *	RandomSpec.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/25/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Quick
import Nimble
@testable import RoShamBoCore

// MARK: - Definitions -

// MARK: - Type -

class RandomSpec : QuickSpec {

// MARK: - Properties

// MARK: - Protected Methods

// MARK: - Exposed Methods

// MARK: - Overridden Methods

	override func spec() {

		describe("Testing Random") {

			context("With random open range") {

				it("Should return any number between 0 and 10, including those") {
					expect((0...10).contains(Int.random(in: 0...10))).to(beTruthy())
					expect(Int.random(in: 0...10)).toNot(equal(11))
					expect(Int.random(in: 0...10)).toNot(equal(-1))
				}
			}
			
			context("With random closed range") {
				
				it("Should return any number between 0 and 10, excluding 10") {
					expect(Int.random(in: 0..<10)).toNot(equal(10))
					expect(Int.random(in: 0..<10)).toNot(equal(-1))
				}
			}
			
			context("With random RoShamBo action") {
				
				it("Should always be one of the valid cases") {
					expect(RoShamBoAction.allCases.contains(RoShamBoAction.random)).to(beTruthy())
					expect(RoShamBoAction.allCases.contains(RoShamBoAction.random)).to(beTruthy())
					expect(RoShamBoAction.allCases.contains(RoShamBoAction.random)).to(beTruthy())
				}
			}
		}
	}
}
