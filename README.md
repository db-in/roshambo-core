# RoShamBoCore

![POD](https://img.shields.io/badge/Coverage-100%25-green.svg)
![POD](https://img.shields.io/badge/swift-3.2-blue.svg)

# Description
**RoShamBoCore** is a framework for ... `make an introduction for this component. Explaining the major business goals.`

**Features**

* Feature 1
* Feature 2
* Feature 3

# Installation

#### CocoaPods:
[CocoaPods](https://guides.cocoapods.org/using/getting-started.html) is a dependency manager for Cocoa projects. You can get more information about how to use it on [Using CocoaPods](https://guides.cocoapods.org/using/using-cocoapods.html)

Once you ready with CocoaPods, use this code in your `Podfile`:

```
source 'https://git.db-in.com/scm/ema/cocoapodsspecs.git'

use_frameworks!

pod 'RoShamBoCore'
```

# Requirements
Version | Language | Xcode | iOS
------- | -------- | ----- | ---
 1.0.0  | Swift 3.2  |  9.0  | 10.0

# Programming Guide
This section intends to give an overview about this component and its usage. Not all features included in this component will be covered here. The focus of this section will be:

* Initialization
* Feature-1
* Feature-2
* Feature-3

#### Initialization
Start by importing the package in the file you want to use it.

```
import RoShamBoCore
```

#### Feature-1
Describe usage of Feature-1

```
// Some code for Feature-1
```

# FAQ
> Possible Question-1?

- Answer for Question-1
