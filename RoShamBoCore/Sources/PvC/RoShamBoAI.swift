/*
 *	RoShamBoAI.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/26/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Foundation

// MARK: - Definitions -

// MARK: - Type -

public class RoShamBoAI {

// MARK: - Properties

	private(set) var results: [RoShamBoDuoMatch] = []
	private var predictionMachine = Prediction()
	
	var nextAIMove: RoShamBoAction {
		return predictionMachine.predict(on: results)
	}
	
// MARK: - Constructors

// MARK: - Protected Methods

// MARK: - Exposed Methods
	
	public func play(against playerMove: RoShamBoAction) -> RoShamBoDuoMatch {
		let result = (p1: playerMove, p2: nextAIMove)
		results.append(result)
		return result
	}
	
// MARK: - Overridden Methods
}
