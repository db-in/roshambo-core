/*
 *	Prediction.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/26/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Foundation
import MarkovModel

// MARK: - Definitions -

// MARK: - Type -

class Prediction {

// MARK: - Properties

	let algorithims: [Prediction] = [MarkovPrediction(), LastBehaviourPrediction()]
	
// MARK: - Constructors

// MARK: - Protected Methods

// MARK: - Exposed Methods
	
	func predict(on results: [RoShamBoDuoMatch]) -> RoShamBoAction {
	
		guard results.count > 2 else {
			return RoShamBoAction.random
		}
		
		var score = [Int](repeating: 0, count: algorithims.count)
		
		(2..<results.count).forEach { index in
			let nextMove = results[index].p1
			let transitions = Array(results.prefix(upTo: index))
			let currentScore = algorithims.map { $0.predict(on: transitions) == nextMove ? 1 : 0 }
			
			score = (0..<score.count).map { score[$0] + currentScore[$0] }
		}
		
		return .paper
	}

// MARK: - Overridden Methods

}

private class LastBehaviourPrediction : Prediction {
	
	override func predict(on results: [RoShamBoDuoMatch]) -> RoShamBoAction {
		
		guard let lastPlay = results.last  else {
			return RoShamBoAction.random
		}
		
		switch lastPlay {
		case let play where play.p1 > play.p2:
			return play.p1.losesTo
		case let play where play.p1 < play.p2:
			return play.p2
		case let play where play.p1 == play.p2:
			return play.p2.losesTo
		default:
			return RoShamBoAction.random
		}
	}
}

private class MarkovPrediction : Prediction {
	
	override func predict(on results: [RoShamBoDuoMatch]) -> RoShamBoAction {
		
		var nextMode = RoShamBoAction.random
		let player = results.map { $0.p1 }
		
		guard let lastPlay = results.last  else {
			return nextMode
		}
		
		MarkovModel.process(transitions: player) {
			nextMode = $0.next(given: lastPlay.p1) ?? nextMode
		}
		
		return nextMode
	}
}
