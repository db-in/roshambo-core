/*
 *	RoShamBoAction.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/24/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Foundation

// MARK: - Definitions -

// MARK: - Type -

public enum RoShamBoAction {
	
	case rock
	case paper
	case scissors
	
// MARK: - Properties
	
	public static let allCases: [RoShamBoAction] = [.rock, .paper, .scissors]

// MARK: - Constructors

// MARK: - Protected Methods

// MARK: - Exposed Methods
	
	public var winsFrom: RoShamBoAction {
		switch self {
		case .rock:
			return .scissors
		case .scissors:
			return .paper
		case .paper:
			return .rock
		}
	}
	
	public var losesTo: RoShamBoAction {
		switch self {
		case .rock:
			return .paper
		case .scissors:
			return .rock
		case .paper:
			return .scissors
		}
	}

// MARK: - Overridden Methods

}
