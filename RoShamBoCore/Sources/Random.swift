/*
 *	Random.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/24/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Foundation

// MARK: - Definitions -

// MARK: - Type -

public extension Int {

// MARK: - Properties

// MARK: - Constructors

// MARK: - Protected Methods

// MARK: - Exposed Methods
	
	/// Returns a random number inside the open range.
	///
	/// - Parameter range: The given range.
	/// - Returns: A ramdom number that lies inside the specified range.
	public static func random(in range: Range<Int>) -> Int {
		return random(range.lowerBound, range.upperBound - 1)
	}
	
	/// Returns a random number inside the closed range.
	///
	/// - Parameter range: The given range.
	/// - Returns: A ramdom number that lies inside the specified range.
	public static func random(in range: ClosedRange<Int>) -> Int {
		return random(range.lowerBound, range.upperBound)
	}
	
	/// Returns a random number inside a given a minimum and maximum.
	///
	/// - Parameters:
	///   - lower: The lowest acceptable ramdom number, including itself.
	///   - upper: The highest acceptable ramdom number, including itself.
	/// - Returns: A ramdom number that lies inside the specified lower and upper bound.
	private static func random(_ lower: Int = 0, _ upper: Int = 100) -> Int {
		return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
	}
	
// MARK: - Overridden Methods
	
}

// MARK: - Extension - RoShamBoAction

public extension RoShamBoAction {
	
	static var random: RoShamBoAction {
		return RoShamBoAction.allCases[Int.random(in: 0..<RoShamBoAction.allCases.count)]
	}
}
