/*
 *	RoShamBoPlay.swift
 *	RoShamBoCore
 *
 *	Created by Diney Bomfim on 6/26/18.
 *	Copyright 2018 db-in. All rights reserved.
 */

import Foundation

// MARK: - Definitions -

public typealias RoShamBoDuoMatch = (p1: RoShamBoAction, p2: RoShamBoAction)
//public typealias RoShamBoTrioMatch = (p1: RoShamBoAction, p2: RoShamBoAction, p3: RoShamBoAction)

// MARK: - Extension - RoShamBoAction

extension RoShamBoAction: Comparable {
	
	public static func < (lhs: RoShamBoAction, rhs: RoShamBoAction) -> Bool {
		switch rhs {
		case lhs.winsFrom:
			return false
		case lhs.losesTo:
			return true
		default:
			return false
		}
	}
}
