/*
 *  RoShamBoCore.h
 *  RoShamBoCore
 *
 *  Created by DINEY B ALVES on 6/24/18.
 *  Copyright 2018 db-in. All rights reserved.
 */

#import <Foundation/Foundation.h>

// MARK: - Definitions -

// Framework identification.
static const double kRoShamBoCoreVersion = 1.0;
static NSString *const kRoShamBoCoreName = @"RoShamBoCore";
