/*
 *  ModuleIntegrationTestsSpecs.swift
 *  RoShamBoCore
 *
 *  Created by DINEY B ALVES on 6/24/18.
 *  Copyright 2018 db-in. All rights reserved.
 */

import Quick
import Nimble
@testable import RoShamBoCore

// MARK: - Definitions -

// MARK: - Type -

class ModuleIntegrationTestsSpecs : QuickSpec {

// MARK: - Properties

// MARK: - Protected Methods

// MARK: - Exposed Methods

// MARK: - Overridden Methods

  override func spec() {

    describe("WHAT is being tested") {

      context("HOW is going to be tested, under which state/condition") {

        it("WHY is this test being done, explain the expected behavior/result") {
          expect("string").to(equal("string"))
        }
      }
    }
  }
}
