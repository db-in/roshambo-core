//: [Previous](@previous)

import Foundation

//public String getPlayerId() {
//	return "Iocaine Powder";
//}
//
//public String getAuthor() {
//	return "Dan Egnor";
//}

//public class IocainePlayer {
//
//	static let ages = [1000, 100, 10, 5, 2, 1]
//	static let num_ages = ages.count
//
//	static let my_hist = 0
//	static let opp_hist = 1
//	static let both_hist = 2
//
//	var my_history: [Int] = []
//	var opp_history: [Int] = []
//
//	var lastmove: Int
//	var trials: Int
//
//	var bestmove: [Int] = []
//	var best_length: [Int] = []
//
//	var mres: result
//
//	var pr_history: [[[predict]]]
//	var pr_freq: [[predict]]
//	var pr_meta: [predict]
//
//	var pr_fixed: predict
//	var pr_random: predict
//
//	var stats: [stat]
//
//	// inner class result for stat.max and predict.do_predict
//
//	class result {
//
//		var move: Int
//		var score: Int
//	}
//
//	// Inner clas Stat
//
//	class stat {
//
//		var sum: [[Int]]
//		var age: Int
//
//		init() {
//			sum = [ 1 + trials ][ 3 ]
//			age = 0;
//			for ( int i = 0; i < 3; ++i ) {
//				sum[ age ][ i ] = 0;
//			}
//		}
//
//		func add(i: Int, delta: Int) {
//			sum[ age ][ i ] += delta;
//		}
//
//		func next() {
//			if ( age < trials ) {
//			age += 1
//				for ( int i = 0; i < 3; ++i ) {
//					sum[ age ][ i ] = sum[ age - 1 ][ i ]
//				}
//			}
//		}
//
//		boolean max( int dage, result res ) {
//
//		res.move = -1;
//		for ( int i = 0; i < 3; ++i ) {
//		int diff;
//		if ( dage > age )
//		diff = sum[ age ][ i ];
//		else
//		diff = sum[ age ][ i ] - sum[ age - dage ][ i ];
//		if ( diff > res.score ) {
//		res.score = diff;
//		res.move = i;
//		}
//		}
//		return ( -1 != res.move );
//		}
//
//	} // end class stats
//
//	// inner class predict
//
//	class predict {
//
//		var st: stat
//		var last: Int
//
//		init() {
//			st = new stat()
//			last = -1
//		}
//
//		func do_predict( int olast, int guess ) {
//		// olast: opponent's last move (-1 if none)
//		// guess: algorithm's prediction of opponent's next move
//
//			if ( -1 != olast ) {
//				int diff = ( 3 + ( olast % 3 ) - ( last % 3 ) ) % 3;
//				st.add( ( diff + 1 ) % 3, 1 ); // will_beat...
//				st.add( ( diff + 2 ) % 3, -1 ); // will loose
//				st.next();
//			}
//			last = guess;
//		}
//
//		func scan( int age, result res ) {
//			int storemove = res.move;
//			if ( st.max( age, res ) ) {
//				res.move = ( ( last + res.move ) % 3 );
//			} else {
//				res.move = storemove;
//			}
//		}
//
//	} // end inner class predict
//
//	// History algorithm
//
//	int match_single( int i, int num, int[] history ) {
//	int high = num;
//	int low = i;
//	while ( low > 0 && history[ low ] == history[ high ] ) {
//	low--;
//	high--;
//	}
//	return num - high;
//	}
//
//	int match_both( int i, int num ) {
//	int j;
//	for ( j = 0; ( j < i ) && ( opp_history[ num - j ] == opp_history[ i - j ] )
//	&& ( my_history[ num - j ] == my_history[ i - j ] ); ++j ) {
//	// Philip Tucker - all logic is in for loop
//	}
//	return j;
//	}
//
//	void do_history( int age, int best[] ) {
//	int num = my_history[ 0 ];
//	for ( int w = 0; w < 3; ++w )
//	best[ w ] = best_length[ w ] = 0;
//	for ( int i = num - 1; i > num - age && i > best_length[ my_hist ]; --i ) {
//	int j = match_single( i, num, my_history );
//	if ( j > best_length[ my_hist ] ) {
//	best_length[ my_hist ] = j;
//	best[ my_hist ] = i;
//	if ( j > num / 2 )
//	break;
//	}
//	}
//
//	for ( int i = num - 1; i > num - age && i > best_length[ opp_hist ]; --i ) {
//	int j = match_single( i, num, opp_history );
//	if ( j > best_length[ opp_hist ] ) {
//	best_length[ opp_hist ] = j;
//	best[ opp_hist ] = i;
//	if ( j > num / 2 )
//	break;
//	}
//	}
//
//	for ( int i = num - 1; i > num - age && i > best_length[ both_hist ]; --i ) {
//	int j = match_both( i, num );
//	if ( j > best_length[ both_hist ] ) {
//	best_length[ both_hist ] = j;
//	best[ both_hist ] = i;
//	if ( j > num / 2 )
//	break;
//	}
//	}
//	}
//
//	/**
//	* @see com.anji.tournament.Player#reset()
//	*/
//	public void reset() {
//	reset( trials );
//	}
//
//	/**
//	* @see com.anji.roshambo.RoshamboPlayer#reset(int)
//	*/
//	public void reset( int aTrials ) {
//
//	// allocate all memory needed...
//
//	my_history = new int[ aTrials + 1 ];
//	opp_history = new int[ aTrials + 1 ];
//	my_history[ 0 ] = opp_history[ 0 ] = 0;
//	lastmove = 0;
//	this.trials = aTrials;
//
//	pr_history = new predict[ num_ages ][ 3 ][ 2 ];
//	pr_freq = new predict[ num_ages ][ 2 ];
//	pr_meta = new predict[ num_ages ];
//	stats = new stat[ 2 ];
//
//	for ( int i = 0; i < num_ages; i++ ) {
//	pr_meta[ i ] = new predict();
//	for ( int j = 0; j < 2; j++ ) {
//	pr_freq[ i ][ j ] = new predict();
//	for ( int k = 0; k < 3; k++ )
//	pr_history[ i ][ k ][ j ] = new predict(); // order i-k-j!
//	}
//	}
//	stats[ 0 ] = new stat();
//	stats[ 1 ] = new stat();
//	pr_fixed = new predict();
//	pr_random = new predict();
//	mres = new result();
//
//	bestmove = new int[ 3 ];
//	best_length = new int[ 3 ];
//	}
//
//	/**
//	* @see com.anji.roshambo.RoshamboPlayer#storeMove(int, int)
//	*/
//	public void storeMove( int move, int score ) {
//	opp_history[ ++opp_history[ 0 ] ] = move;
//	my_history[ ++my_history[ 0 ] ] = lastmove;
//	}
//
//	private int getRandomMove() {
//	int randomMove = (int) Math.round(Math.random() * 2);
//	return randomMove;
//	}
//
//	/**
//	* @see com.anji.roshambo.RoshamboPlayer#nextMove()
//	*/
//	public int nextMove() {
//
//	int num = my_history[ 0 ];
//	int last = ( num > 0 ) ? opp_history[ num ] : -1;
//	int guess = getRandomMove();
//
//	if ( num > 0 ) {
//	stats[ 0 ].add( my_history[ num ], 1 );
//	stats[ 1 ].add( opp_history[ num ], 1 );
//	}
//
//	for ( int a = 0; a < num_ages; ++a ) {
//	do_history( ages[ a ], bestmove );
//	for ( int w = 0; w < 3; ++w ) {
//	int b = bestmove[ w ];
//	if ( 0 == b ) {
//	pr_history[ a ][ w ][ 0 ].do_predict( last, guess );
//	pr_history[ a ][ w ][ 1 ].do_predict( last, guess );
//	continue;
//	}
//	pr_history[ a ][ w ][ 0 ].do_predict( last, my_history[ b + 1 ] );
//	pr_history[ a ][ w ][ 1 ].do_predict( last, opp_history[ b + 1 ] );
//	}
//
//	for ( int p = 0; p < 2; ++p ) {
//	mres.score = -1;
//	if ( stats[ p ].max( ages[ a ], mres ) )
//	pr_freq[ a ][ p ].do_predict( last, mres.move );
//	else
//	pr_freq[ a ][ p ].do_predict( last, guess );
//	}
//	}
//
//	pr_random.do_predict( last, guess );
//	pr_fixed.do_predict( last, 0 );
//
//	for ( int a = 0; a < num_ages; ++a ) {
//	mres.score = -1;
//	mres.move = -1;
//
//	for ( int aa = 0; aa < num_ages; ++aa ) {
//	for ( int p = 0; p < 2; ++p ) {
//	for ( int w = 0; w < 3; ++w ) {
//	pr_history[ aa ][ w ][ p ].scan( ages[ a ], mres );
//	}
//	pr_freq[ aa ][ p ].scan( ages[ a ], mres );
//	}
//	}
//
//	pr_random.scan( ages[ a ], mres );
//	pr_fixed.scan( ages[ a ], mres );
//	pr_meta[ a ].do_predict( last, mres.move );
//	}
//
//	mres.score = -1;
//	mres.move = -1;
//
//	for ( int a = 0; a < num_ages; ++a ) {
//	pr_meta[ a ].scan( trials, mres );
//	}
//
//	lastmove = mres.move;
//	return mres.move;
//	}
//}
