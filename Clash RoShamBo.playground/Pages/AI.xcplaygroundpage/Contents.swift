import UIKit
import RoShamBoCore

//extension Sequence {
//
//	public func groupBy<T: Hashable>(_ getKey: (Iterator.Element) -> T) -> [T : [Iterator.Element]] {
//		var grouped = [T : Array<Iterator.Element>]()
//
//		forEach { element in
//			let key = getKey(element)
//
//			if var array = grouped[key] {
//				array.append(element)
//				grouped[key] = array
//			} else {
//				grouped[key] = [element]
//			}
//		}
//
//		return grouped
//	}
//
//	public func groupBy<T: Hashable>(_ getKey: (Iterator.Element) -> T) -> [T : [Iterator.Element]] {
//		var grouped = [T : Array<Iterator.Element>]()
//
//		forEach { element in
//			let key = getKey(element)
//
//			if var array = grouped[key] {
//				array.append(element)
//				grouped[key] = array
//			} else {
//				grouped[key] = [element]
//			}
//		}
//
//		return grouped
//	}
//}

//extension RandomAccessCollection where Self : MutableCollection {
//	
//	/// Returns a new collection with all its elements suffled randomly.
//	///
//	///     let numbers = [10, 20, 30, 40, 50]
//	///     print(numbers.shuffle())
//	///
//	/// - Complexity: O(n)
//	public mutating func shuffle() {
//		let n = count
//		guard n > 1 else { return }
//		for (i, pos) in indices.dropLast().enumerated() {
//			let otherPos = index(startIndex, offsetBy: Int.random(in: i..<n))
//			swapAt(pos, otherPos)
//		}
//	}
//}

//var model = MarkovChain(states: ["dog", "cat", "bear"])
//
//model.currentState = "dog"
//model.currentState = "cat"
//model.currentState = "dog"
//model.currentState = "bear"
//model.currentState = "dog"
//model.currentState = "dog"
//model.currentState = "dog"
//model.currentState = "bear"
//model.currentState = "cat"
//model.currentState = "dog"
//model.currentState = "dog"
//model.currentState = "dog"
//model.currentState = "bear"
//model.currentState = "cat"
//model.currentState = "dog"
//model.currentState = "bear"
//model.currentState = "bear"
//model.currentState = "cat"
//print(model)
//print("----")
//model.buildMatrix()
//print("----")
//print(model)

var v = MarkovChain(transitions: ["dog", "cat", "dog", "bear", "dog", "dog", "dog", "bear", "cat", "dog", "dog", "dog", "bear", "cat", "dog", "bear", "bear", "cat"])
print(v)

var z = MarkovChain(states: ["dog", "cat", "bear"])
print(z)

//0.444 1.000 0.200
//0.111 0.000 0.600
//0.444 0.000 0.200
